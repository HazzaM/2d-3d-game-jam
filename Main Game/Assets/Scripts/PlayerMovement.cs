﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    float stamina = 5, maxStamina = 5;

    public float speed;
    public float walkSpeed = 6f;
    public float runSpeed = 12f;
    public bool isRunning;

    public float gravity = -19.62f;
    public float jumpHeight = 1.5f;

    //Variables for ground checking + setting a layermask for terrain/objects
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;

    bool isGrounded;

    void Update()
    {
        //Checking if the player is grounded, this allows for consistency in simulating gravity.
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2.0f;
        }

        //Calling the axis inputs inside Unity
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        //Movement direction, speed of movement * deltaTime
        controller.Move(move * speed * Time.deltaTime);
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            
        }

        //Left shift to run + plays running animation
        if (Input.GetButtonDown("LeftShift") && isGrounded)
        {
            
            speed = runSpeed;
            isRunning = true;

        }

        //Key up to end running + animation ends
        if (Input.GetButtonUp("LeftShift") && isGrounded)
        {
            
            speed = walkSpeed;
            isRunning = false;
        }

        if (isRunning)
        {
            stamina -= Time.deltaTime;
            if (stamina < 0)
            {
                stamina = 0;
                speed = walkSpeed;
                isRunning = false;
            }
        }
        else if (stamina < maxStamina)
        {
            stamina += Time.deltaTime;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}
