﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunPickup : MonoBehaviour
{

    public GameObject PlayerGun = null;
    public GameObject GunAlert = null;

    // Start is called before the first frame update
    void Start()
    {
        PlayerGun.SetActive(false);
        GunAlert.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {   

        if (other.name == "Player")
        {
            Destroy(gameObject);
            PlayerGun.SetActive(true);
            GunAlert.SetActive(true);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
