﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public GameObject FPCamera;
    public GameObject TPCamera;
    public int CameraMode;

    void Update()
    {
        
        if (Input.GetButtonDown("CameraKey")) 
        {
            if (CameraMode == 1) 
            { 
                CameraMode = 0; 
            } else { 
                CameraMode += 1; 
            }
            StartCoroutine(CameraChange());
        } 
    }

    IEnumerator CameraChange()
    {
        yield return new WaitForSeconds(0.01f);
        if (CameraMode == 0)
        {
            FPCamera.SetActive(true);
            TPCamera.SetActive(false);
        }
        if (CameraMode == 1)
        {
            TPCamera.SetActive(true);
            FPCamera.SetActive(false);
        }
    }
}
