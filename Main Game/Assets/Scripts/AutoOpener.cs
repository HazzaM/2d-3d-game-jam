﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoOpener : MonoBehaviour
{
    private Animator Eanimator;

    // Start is called before the first frame update
    void Start()
    {
        Eanimator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Eanimator.SetBool("open1", true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Eanimator.SetBool("open1", false);
        
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
