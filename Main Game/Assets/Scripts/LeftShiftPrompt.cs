﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftShiftPrompt : MonoBehaviour
{
    public GameObject ShiftPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        ShiftPanel.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ShiftPanel.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            ShiftPanel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
