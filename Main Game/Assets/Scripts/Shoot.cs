﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float damage = 5f;
    public float range = 100f;

    public int maxAmmo = 3;
    private int currentAmmo;
    public float reloadTime = 1f;
    private bool isReloading = false;

    public Camera fpsCam;

    private Animator dAnimator;
    public ParticleSystem MuzzleFlash;
 

    AudioSource n_shootingSound;


    // Start is called before the first frame update
    void Start()
    {
        dAnimator = GetComponent<Animator>();
        n_shootingSound = GetComponent<AudioSource>();
        n_shootingSound.Stop();
        currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        if (isReloading) return;

        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetMouseButton(0))
        {
            Shooting();
            n_shootingSound.Play();
            dAnimator.SetBool("fire", true);
            MuzzleFlash.Emit(1);
        }
        else 
        {
            dAnimator.SetBool("fire", false);
        }
        
    }

    IEnumerator Reload()
    {
        isReloading = true;

        Debug.Log("Reload Weapon");
        yield return new WaitForSeconds(reloadTime);
        currentAmmo = maxAmmo;
        isReloading = false;
    }

    void Shooting()
    {
        currentAmmo--;
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
           
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
        }
    }
}
